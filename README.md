# EEE3096S Embedded systems 2
## Group: RSSLLO001(Lloyd Ross) and LNDCAI001(Caide Lander)

This repository stores all necessary files and documents for all group work and projects for Embedded Systems 2

# Folders
## Prac 1
This folder contains the main.c file of Tutorial 1's code

## Prac 2
This folder contains the main.c file of Practical 2's code
